### IMPORTANTE
  Não consegui ter acesso a API dada pelo postman, nem mesmo logar pelo usuário pre-setado dentro do arquivo postman.
  Então criei um mockup para poder renderizar de forma automática, a action de autenticar já está pronta, porém como eu não
  conseguia retornar dados, não completei a action de pegar os dados vindo do axios.
  
  Prints no final do README

---

## Login
![loginPage](https://user-images.githubusercontent.com/53899920/67149317-50fcef00-f280-11e9-942f-624d90c284ee.png)

## Main page
![mainPage1](https://user-images.githubusercontent.com/53899920/67149318-50fcef00-f280-11e9-853a-85826c9eb9fb.png)
![mainPage2](https://user-images.githubusercontent.com/53899920/67149319-50fcef00-f280-11e9-8acd-3548a1be0f10.png)

## Information page
![infoPage](http://user-images.githubusercontent.com/53899920/67149320-51958580-f280-11e9-82e9-bddc6cfddde5.png)




### Como instalar:
* git clone https://theviggo@bitbucket.org/theviggo/empresas-react-native.git
* yarn
* cd ios
* pod install

### Dependencias  
+ Axios  
      * Usado para recuperar dados da api  
  
+ React-redux
+ Redux
+ Redux-saga
+ React-redux  
      * Alterar/Receber/Criar states  
      
  
* Reactotron-react-native
* Reactotron-redux
* Reactotron-redux-saga  
      + Monitorar requests do saga, console.log, states,etc  
      
  
* Styled-components
      * Componentes personalizados para melhor estilização e padronização  
  
  
* Linear Gradient
      * Usado para personalizar a cor do botão da page SignIn  
  
  
* Vector Icons
      * Usado para inserir icones pré-criadas  
  
  
* Eslint/Prettier
      * Linter  
  
  
* Prop Types
      * Usado para estilizar o Input de forma dinâmica.  
  
## Erro na API
 
![error1](https://user-images.githubusercontent.com/53899920/67149321-51958580-f280-11e9-88ad-027f2c1aecea.jpeg)
![error2](https://user-images.githubusercontent.com/53899920/67149322-51958580-f280-11e9-9181-d79529dada13.jpeg)  
* Mesmo erro



