import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import SignIn from './pages/SignIn';
import Main from './pages/Main';
import Information from './pages/Information';

export default (isSigned = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        SignIn,
        Main,
        Information,
      },
      {
        initialRouteName: isSigned ? 'Main' : 'SignIn',
      }
    )
  );
