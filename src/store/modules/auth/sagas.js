import { takeLatest, call, put, all } from 'redux-saga/effects';
import api from '../../../services/api';

import { signInSuccess } from './actions';

export function* signIn({ payload }) {
  const { email, password } = payload;

  const response = yield call(api.post, 'api/v1/users/auth/sign_in', {
    email,
    password,
  });

  const something = response.data;
  // como eu não sei os dados que irão retornar, ainda não os desconstrui.

  yield put(signInSuccess(something));
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);
