import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

export const Container = styled(RectButton)`
  height: 50px;
  align-items: center;
  justify-content: center;
`;

export const Gradient = styled(LinearGradient).attrs({
  colors: ['#E51F6B', '#E52981'],
})`
  border-radius: 4px;
  align-self: stretch;
`;

export const Text = styled.Text`
  color: #fff;
  font-weight: bold;
  font-size: 16px;
`;
