import React from 'react';
import { ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';

import { Container, Text, Gradient } from './styles';

export default function Button({ children, loading, ...rest }) {
  return (
    <Gradient>
      <Container {...rest}>
        {loading ? (
          <ActivityIndicator size="small" color="FFF" />
        ) : (
          <Text>{children}</Text>
        )}
      </Container>
    </Gradient>
  );
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  loading: PropTypes.bool,
};

Button.defaultProps = {
  loading: false,
};
