import styled from 'styled-components/native';

export const Container = styled.View`
  margin-bottom: 15px;
  padding: 20px;
  border-radius: 4px;
  background: #fff;
  border-bottom-width: 0.5;
  border-color: #e8e8e8;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Left = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const Image = styled.Image`
  width: 100px;
  height: 100px;
  border-radius: 5px;
`;
export const Info = styled.View`
  margin-left: 15px;
`;
export const Name = styled.Text`
  font-weight: bold;
  font-size: 14px;
  color: #333;
`;
export const Details = styled.Text`
  color: #999;
  font-size: 13px;
  margin-top: 20px;
`;
