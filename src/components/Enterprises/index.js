import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Container, Left, Image, Info, Name, Details } from './styles';

export default function Enterprises({ navigation, data }) {
  return (
    <Container>
      <Left>
        <Image source={{ uri: `${data.url}` }} />

        <Info>
          <Name>{data.name.toUpperCase()}</Name>
          <Details>{data.details}</Details>
        </Info>
      </Left>

      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Information', { navigation, data });
        }}
      >
        <Icon
          name="keyboard-arrow-right"
          size={30}
          color="#E51F6B"
          style={{ alignSelf: 'flex-end' }}
        />
      </TouchableOpacity>
    </Container>
  );
}
