const mocks = {
  enterprise: [
    {
      id: 0,
      name: 'agro',
      details: 'Agro details',
      url: 'https://images.pexels.com/photos/2218364/pexels-photo-2218364.jpeg',
    },
    {
      id: 1,
      name: 'aviation',
      details: 'Aviation details',
      url: 'https://images.pexels.com/photos/912050/pexels-photo-912050.jpeg',
    },
    {
      id: 2,
      name: 'biotech',
      details: 'Biotech details',
      url: 'https://images.pexels.com/photos/2280547/pexels-photo-2280547.jpeg',
    },
    {
      id: 3,
      name: 'eco',
      details: 'Eco details',
      url: 'https://images.pexels.com/photos/1466434/pexels-photo-1466434.jpeg',
    },
    {
      id: 4,
      name: 'ecommerce',
      details: 'Ecommerce details',
      url: 'https://images.pexels.com/photos/230544/pexels-photo-230544.jpeg',
    },
    {
      id: 5,
      name: 'education',
      details: 'Education details',
      url:
        'https://images.pexels.com/photos/159621/open-book-library-education-read-159621.jpeg',
    },
    {
      id: 6,
      name: 'fashion',
      details: 'Fashion details',
      url: 'https://images.pexels.com/photos/291762/pexels-photo-291762.jpeg',
    },
    {
      id: 7,
      name: 'fintech',
      details: 'Fintech details',
      url: 'https://images.pexels.com/photos/186461/pexels-photo-186461.jpeg',
    },
    {
      id: 8,
      name: 'food',
      details: 'Food details',
      url: 'https://images.pexels.com/photos/842571/pexels-photo-842571.jpeg',
    },
    {
      id: 9,
      name: 'games',
      details: 'Games details',
      url: 'https://images.pexels.com/photos/275033/pexels-photo-275033.jpeg',
    },
  ],
};

export default mocks;
