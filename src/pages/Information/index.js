import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { Container, Image, Title, Details, Info } from './styles';

export default function Information({ navigation }) {
  const { name, details, url } = navigation.getParam('data');

  return (
    <>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Main');
        }}
      >
        <Icon
          name="keyboard-arrow-left"
          size={50}
          color="#E51F6B"
          style={{ alignSelf: 'flex-start', marginTop: 50, marginBottom: 10 }}
        />
      </TouchableOpacity>

      <Container>
        <Image source={{ uri: url }} />
        <Info>
          <Title>{name.toUpperCase()}</Title>
        </Info>
        <Details>{details}</Details>
      </Container>
    </>
  );
}
