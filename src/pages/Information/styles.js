import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
`;

export const Image = styled.Image`
  height: 300px;
  width: 90%;
`;
export const Title = styled.Text`
  margin: 10px 0;
  font-size: 32px;
  color: #222;
`;
export const Details = styled.Text`
  margin-top: 10px;
  font-size: 16px;
  color: #333;
`;

export const Info = styled.View`
  border-color: #e51f6b;
  border-bottom-width: 1px;
  padding: 0 20%;
`;
