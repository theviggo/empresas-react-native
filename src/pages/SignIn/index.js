import React, { useState } from 'react';
// import { useDispatch } from 'react-redux';
import logo from '../../assets/logo_ioasys.png';

// import { signInRequest } from '../../store/modules/auth/actions';

import { Container, Form, FormInput, SubmitButton, Image } from './styles';

export default function SignIn({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // const dispatch = useDispatch();

  function handleSubmit() {
    // dispatch(signInRequest(email, password));
    // Funçao que autenticaria na api.
    navigation.navigate('Main');
  }

  return (
    <Container>
      <Image source={logo} />

      <Form>
        <FormInput
          icon="mail-outline"
          keyboardType="email-address"
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Digite seu email"
          value={email}
          onChangeText={setEmail}
        />

        <FormInput
          icon="lock-outline"
          secureTextEntry
          placeholder="Digite sua senha"
          value={password}
          onChangeText={setPassword}
        />
      </Form>

      <SubmitButton onPress={handleSubmit}>Acessar</SubmitButton>
    </Container>
  );
}
