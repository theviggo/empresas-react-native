import styled from 'styled-components/native';
import Input from '../../components/Input';

export const Container = styled.SafeAreaView`
  flex: 1;
`;

export const SearchBar = styled(Input).attrs({
  placeholderTextColor: '#7159c1',
})`
  margin: 30px 30px 0;
  padding: 10px;
  background-color: #e8e8e8;
`;

export const Title = styled.Text`
  font-size: 24px;
  color: #e51f6b;
  font-weight: bold;
  align-self: center;
  margin-top: 30px;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: { padding: 10 },
})``;
