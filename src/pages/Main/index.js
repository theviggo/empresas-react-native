import React, { useState, setState } from 'react';
import { Container, Title, List, SearchBar } from './styles';

import Enterprises from '../../components/Enterprises';

import mocks from '../../assets/mocks';

export default class Main extends React.Component {
  state = {
    keyword: '',
    data: mocks.enterprise,
  }

  render() {
    const { keyword, data } = this.state;

    filterResults = keyword => {
      this.setState({ data: data.filter(item => item.name.includes(keyword))})
      if(!keyword) this.setState({ data: mocks.enterprise })
    }

    return (
      <Container>
        <Title>Empresas</Title>
        <SearchBar
          placeholder="Procurar empresa"
          icon="search"
          autoCorrect={false}
          autoCapitalize="none"
          value={keyword}
          onChangeText={(keyword) => {
            this.setState({ keyword })
            filterResults(keyword)
          }}
        />

        <List
          data={data}
          keyExtractor={item => String(item.id)}
          renderItem={({ item }) => <Enterprises navigation={this.props.navigation} data={item} />}
        /> 
      </Container>
    );
  }
}
